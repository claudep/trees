from django.db import migrations, models
import trees.models


class Migration(migrations.Migration):

    dependencies = [
        ('trees', '0002_reference_photo_1'),
    ]

    operations = [
        migrations.AddField(
            model_name='reference',
            name='photo_2',
            field=trees.models.ImageFieldThumb(blank=True, null=True, upload_to='photos', verbose_name='Photo 2'),
        ),
        migrations.AddField(
            model_name='reference',
            name='photo_3',
            field=trees.models.ImageFieldThumb(blank=True, null=True, upload_to='photos', verbose_name='Photo 3'),
        ),
        migrations.AddField(
            model_name='reference',
            name='pub_text',
            field=models.TextField(blank=True, verbose_name='Texte pour publication'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='photo_1',
            field=trees.models.ImageFieldThumb(blank=True, null=True, upload_to='photos', verbose_name='Photo 1'),
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trees', '0003_photos_amd_pubtext'),
    ]

    operations = [
        migrations.CreateModel(
            name='Canton',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('archived', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50, verbose_name='Nom')),
                ('abrev', models.CharField(max_length=10, verbose_name='Abréviation')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='reference',
            name='prive',
            field=models.BooleanField(default=False, verbose_name='Propriété privée'),
        ),
        migrations.AddField(
            model_name='reference',
            name='rue',
            field=models.CharField(blank=True, max_length=150, verbose_name='Rue'),
        ),
        migrations.AddField(
            model_name='reference',
            name='canton',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to='trees.Canton'),
        ),
    ]

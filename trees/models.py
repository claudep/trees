import os

from PIL import Image

from django.conf import settings
from django.db import models

from common.models import BaseReference, ExtMixin
from common.utils import thumbnail_file_name


class ImageFieldThumb(models.ImageField):
    has_thumb_cache = True

    def pre_save(self, model_instance, add):
        file_ = super().pre_save(model_instance, add)
        if file_:
            image_path = file_.path
            new_name = thumbnail_file_name(image_path)
            new_path = os.path.join(
                settings.MEDIA_ROOT, settings.THUMBNAILS_CACHE_DIR, new_name
            )
            if not os.path.exists(new_path):
                # Generate thumbnail
                image = Image.open(image_path)
                image.thumbnail((400, 400), Image.ANTIALIAS)
                image.save(new_path)
        return file_


class Canton(ExtMixin, models.Model):
    name = models.CharField("Nom", max_length=50)
    abrev = models.CharField("Abréviation", max_length=10)


class Reference(BaseReference):
    localite = models.CharField("Localité", max_length=50)
    adresse = models.CharField("Adresse, lieu-dit", max_length=150)
    rue = models.CharField("Rue", max_length=150, blank=True)
    codepostal = models.CharField("Code postal", max_length=6, blank=True)
    canton = models.ForeignKey(Canton, blank=True, null=True, on_delete=models.PROTECT)
    proprietaire = models.CharField("Adresse, lieu-dit", max_length=150, blank=True)
    prive = models.BooleanField("Propriété privée", default=False)
    nom_arbre = models.CharField("Genre de l'arbre", max_length=100)
    taxon = models.CharField("Nom scientifique", max_length=100)
    circ_base = models.SmallIntegerField("Circ. à la base", null=True, blank=True)
    circ_1m = models.SmallIntegerField("Circ. à 1m", null=True, blank=True)
    circ_1m30 = models.SmallIntegerField("Circ. à 1m30", null=True, blank=True)
    circ_1m50 = models.SmallIntegerField("Circ. à 1m50", null=True, blank=True)
    circ_taille = models.SmallIntegerField("Circ. taille", null=True, blank=True)
    hauteur = models.CharField("Hauteur", max_length=20, blank=True)
    diam_couronne = models.SmallIntegerField("Diamètre couronne (m)", null=True, blank=True)
    age = models.CharField("Âge", max_length=20, blank=True)
    commentaire = models.TextField("Notes/Commentaires", blank=True)
    pub_text = models.TextField("Texte pour publication", blank=True)
    coord_long = models.CharField("Longitude", max_length=10, blank=True)
    coord_lat = models.CharField("Latitude", max_length=10, blank=True)
    altitude = models.SmallIntegerField("Altitude", null=True, blank=True)
    photo_1 = ImageFieldThumb("Photo 1", blank=True, null=True, upload_to='photos')
    photo_2 = ImageFieldThumb("Photo 2", blank=True, null=True, upload_to='photos')
    photo_3 = ImageFieldThumb("Photo 3", blank=True, null=True, upload_to='photos')

    _grouped_fields = [
        ('Titre', ('nom_arbre', 'taxon'), 'left'),
        ('Emplacement', ('localite', 'rue', 'codepostal', 'adresse', 'prive', 'canton',
            'coord_long', 'coord_lat', 'altitude'), 'left'),
        ('Photo', ('photo_1', 'photo_2', 'photo_3'), 'right'),
        ('Mensuration', ('circ_base', 'circ_1m', 'circ_1m30', 'circ_1m50', 'circ_taille',
            'hauteur', 'diam_couronne'), 'right'),
        ('Notes', ('age', 'commentaire', 'pub_text'), 'left'),
    ]
    search_fields = ['nom_arbre', 'localite']
    units = {
        'circ_base': '%s cm',
        'circ_1m': '%s cm',
        'circ_1m30': '%s cm',
        'circ_1m50': '%s cm',
        'circ_taille': '%s cm',
        'diam_couronne': '%s m',
    }

    class Meta:
        ordering = ('codepostal', 'nom_arbre')

    def __str__(self):
        return "%s (%s)" % (self.nom_arbre, self.localite)

    @classmethod
    def ref_fields(cls):
        return 'nom_arbre', "nom_arbre"

    @property
    def main_image(self):
        return self.photo_1

    @property
    def all_images(self):
        return [self.photo_1] if self.photo_1 else []

    def add_suffix(self, suffix):
        self.nom_arbre += "-DUPLIQUÉ"

    def pdf_url(self, lang):
        return None

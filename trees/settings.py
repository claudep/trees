import os
from decimal import Decimal

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'mptt',
    'reversion',

    'trees',  # Before the below apps to override templates

    'common',
    'translator.apps.TranslatorConfig',
    'catalog',
    'document',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'common.middleware.LoginRequiredMiddleware',
    'common.middleware.EDMRevisionMiddleware',  # subclass of django-reversion middleware
]

ROOT_URLCONF = 'trees.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'builtins': ['django.contrib.staticfiles.templatetags.staticfiles'],
        },
    },
]

WSGI_APPLICATION = 'trees.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'common.backends.ObjectPermissionBackend',
]

LANGUAGE_CODE = 'fr'
LANGUAGES = (('fr', 'French'),)

TIME_ZONE = 'Europe/Zurich'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'remote_finder.RemoteFinder',
]

DEFAULT_FROM_EMAIL = 'webmaster@2xlibre.net'
SERVER_EMAIL = 'www-data@2xlibre.net'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'access': {
            'format': '%(asctime)s – %(ip)s – %(user)s – %(reference)s – %(message)s',
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'common.logging.MonthRotatingFileHandler',
            'directory': '',  #'/var/log/easydocmaker/<client>/',
            'delay': True,
            'formatter': 'access',
        },
    },
    'loggers': {
        'catalog.views': {
            'level': 'CRITICAL',  # Set to INFO to enable logging
            'handlers': ['file'],
        },
    },
}

# Non-Django settings:

REMOTE_FINDER_CACHE_DIR = os.path.join(BASE_DIR, 'remote-finder-cache')

REMOTE_FINDER_RESOURCES = [
    ('js/vendor/jquery-2.2.4.min.js', 'https://code.jquery.com/jquery-2.2.4.min.js',
     'md5:2f6b11a7e914718e0290410e85366fe9'),
    # jQuery UI
    ('js/vendor/jquery-ui-1.11.4.min.js', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
     'md5:d935d506ae9c8dd9e0f96706fbb91f65'),

    ('css/vendor/jquery-ui-1.11.4.css',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
     'md5:64dfb75ef30cbf691e7858dc1992b4df'),
    ('css/vendor/images/ui-bg_flat_0_aaaaaa_40x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_0_aaaaaa_40x100.png',
     'md5:21f222d245c6229bf1086527335c49e8'),
    ('css/vendor/images/ui-bg_flat_75_ffffff_40x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png',
     'md5:985f46b74b703e605966201743fb6f7e'),
    ('css/vendor/images/ui-bg_highlight-soft_75_cccccc_1x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_highlight-soft_75_cccccc_1x100.png',
     'md5:dd13068fa67603e4ed7302f69af8d905'),
    ('css/vendor/images/ui-bg_glass_65_ffffff_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_65_ffffff_1x400.png',
     'md5:177bd763ea7a4ac8fcb1dc94570dd833'),
    ('css/vendor/images/ui-bg_glass_75_dadada_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_75_dadada_1x400.png',
     'md5:2d6f73d4cc0d5c0e6bb8b05c2ebc1d11'),
    ('css/vendor/images/ui-bg_glass_75_e6e6e6_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png',
     'md5:5c579dd5f9969ffdc83a00007271684c'),
    ('css/vendor/images/ui-icons_888888_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_888888_256x240.png',
     'md5:0979e30214877ac97c47cd1ea71a1058'),
    ('css/vendor/images/ui-icons_222222_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_222222_256x240.png',
     'md5:5b5ec59318bb5f73baf58fcbfeca4e46'),
    ('css/vendor/images/ui-icons_454545_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_454545_256x240.png',
     'md5:aa541bdd3a9d7e85aa84de5342c5712c'),

    ('js/vendor/jquery.form.min.js', 'https://malsup.github.com/min/jquery.form.min.js',
     'md5:f448c593c242d134e9733a84c7a4d26c'),
    ('js/vendor/stupidtable.min.js',
     'https://raw.githubusercontent.com/joequery/Stupid-Table-Plugin/1.0.2/stupidtable.min.js',
     'md5:5916e620b8f7ac48d7458c55a3a4c539'),
    ('js/vendor/js.cookie.js',
     'https://raw.githubusercontent.com/js-cookie/js-cookie/v2.1.3/src/js.cookie.js',
     'md5:893f870eaf4600c848b1f29c66e13917'),
    ('js/vendor/jquery.ajaxQueue.min.js',
     'https://raw.githubusercontent.com/gnarf/jquery-ajaxQueue/master/dist/jquery.ajaxQueue.min.js',
     'md5:e61a21aa43129a8d99a57934f83f3886'),
    # Aloha
    ('js/vendor/require.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.14/require.min.js',
     'md5:9a5abad043df0d348bdc5eb2c200973a'),
    ('js/vendor/aloha/lib/aloha.js', 'https://www.easydocmaker.ch/aloha/lib/aloha.js',
     'md5:9c9af6cf06b613135290cc02f1295ba1'),
    ('js/vendor/aloha/lib/vendor/jquery-1.7.2.js', 'https://code.jquery.com/jquery-1.7.2.min.js',
     'md5:b8d64d0bc142b3f670cc0611b0aebcae'),
    ('js/vendor/aloha/css/aloha.css', 'https://www.easydocmaker.ch/aloha/css/aloha.css',
     'md5:d714776dcf11a97cea0d6750d899df4a'),
    ('js/vendor/aloha/img/arrow.png', 'https://www.easydocmaker.ch/aloha/img/arrow.png',
     'md5:a1ca63c4e624a5f11a4ca3da183f37e6'),
]

# ********* Blackbox specific settings *****************

REF_APP = 'trees'

WATCH_IMAGES_ROOT = os.path.join(BASE_DIR, '<app>/static/img/App')

WATCH_PDF_DIR = 'pdfs'  # Relative to MEDIA_ROOT
SERVE_AUTO_PDF = True

LAYOUTS = (
    {'label': "4/3, 3 lines - 2 columns", 'ratio': Decimal('1.33'), 'num_rows': 3, 'num_cols': 2},
)

EXTERNAL_TRANSLATABLE_DEFAULT = True

EXPORTER_CLASSES = ['catalog.exporter.CatalogExporter']

HOME_IS_DOCUMENTS = False

CATALOG_LANGUAGE_CODE = 'fr'
CATALOG_DEFAULT_LEVELS = [
    {'fields': 'collection', 'common_fields': 'collection'},
    {'fields': 'line', 'common_fields': 'line'},
]

THUMBNAILS_CACHE_DIR = '_thumbs_cache'
CLIENT_CAN_UPLOAD_REF_IMAGES = False
REF_IMAGE_MAX_WIDTH = 500
REF_IMAGE_MAX_HEIGHT = 650
# either landscape, portrait or empty string for no validation
REF_IMAGE_ORIENTATION = 'portrait'
REF_IMAGE_MAX_SIZE = 300*1024
# See https://pillow.readthedocs.org/en/3.0.0/handbook/image-file-formats.html
REF_IMAGE_VALID_FORMATS = ['JPEG']

# IP adresses allowed to view some URLs without login (e.g. xml)
AUTHORIZED_IPS = []
HOTS_IP = '213.221.235.105'
JSON_AUTHORIZED_IPS = [HOTS_IP]

# Flag to toggle edition availability of references in the UI
ENABLE_REF_EDITION = True

# Flag to tell if new references are specially marked in catalogs
USE_ADDITION = True

# Flag to tell if an access QR code should be displayed in reference details
SHOW_QRCODE = False

# Default permission for all groups when creating a new document
DEFAULT_DOC_PERMISSION = 'view_document'  # Could be '' for no access

from .local_settings import *

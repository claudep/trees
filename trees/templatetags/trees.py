from django import template

register = template.Library()


@register.inclusion_tag('geolocation.html')
def geolocation(reference):
    if not reference.coord_long or not reference.coord_lat:
        return {}
    try:
        longitude = int(reference.coord_long.replace(' ', ''))
        latitude = int(reference.coord_lat.replace(' ', ''))
    except ValueError:
        return {}
    return {
        'ref': reference,
        'coords': [longitude, latitude],
    }
